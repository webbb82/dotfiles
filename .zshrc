#!/usr/bin/env bash

#apt package manager
alias aptup='sudo apt update && sudo apt upgrade'
alias aptupd=' sudo apt update'
alias aptupg='sudo apt upgrade'
alias aptin='sudo apt install'
alias aptrm='sudo apt remove'
alias aptupglist='apt list --upgradable'

#Easier disk reading
alias df='df -h'                          
alias free='free -m'                      

#ls/exa
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias l.='exa -a | egrep "^\."'

#Git
alias gaddup='git add -u'
alias gaddall='git add .'
alias gbranch='git branch'
alias gcheckout='git checkout'
alias gclone='git clone'
alias gcommit='git commit -m'
alias gfetch='git fetch'
alias gpull='git pull origin'
alias gpush='git push origin'
alias gstatus='git status'  
alias gtag='git tag'
alias gnewtag='git tag -a'

#Colorized grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#Permissions
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

#NeoVim
alias vim='nvim'

export RANGER_LOAD_DEFAULT_RC=false

#Starship prompt
eval "$(starship init zsh)"

#Neofetch
neofetch

#Anaconda
__conda_setup="$('/home/martin/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/martin/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/martin/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/martin/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup

#Rust programming leaungage
. "$HOME/.cargo/env"


